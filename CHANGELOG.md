# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## v0.3.0-cfork - 2020-05-03
### Added
- Alphabetized bookmark sorting

### Changed
- Kill dmenu at the end of the script to avoid errors
- Versioning


## v0.2.1-cfork - 2020-05-01
### Changed
- Patched new functions

## v0.2.0-cfork - 2020-05-01
### Added
- Tag search feature
- Changelog

### Changed
- Organized code into seperate functions